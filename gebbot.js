const mineflayer = require('mineflayer')
const { pathfinder, Movements, goals } = require('mineflayer-pathfinder')
const { Vec3 } = require('vec3')
const GoalFollow = goals.GoalFollow
const GoalGetToBlock = goals.GoalGetToBlock
const GoalBlock = goals.GoalBlock
const collectBlock = require('mineflayer-collectblock').plugin

if (process.argv.length < 4 || process.argv.length > 6) {
    console.log('Usage : node follow.js <host> <port> [<name>] [<password>]')
    process.exit(1)
  }
  
  const bot = mineflayer.createBot({
    host: process.argv[2],
    port: parseInt(process.argv[3]),
    username: process.argv[4] ? process.argv[4] : 'gebbot',
    password: process.argv[5]
  })

bot.loadPlugin(pathfinder)
bot.loadPlugin(collectBlock)

let mcData;
bot.once('spawn', () => {
  hi()
  mcData = require('minecraft-data')(bot.version)
})

var target = process.argv[4]
var followUser = false

bot.on('chat', (username, message) => {
    const args = message.split(" ")
    if (args[0] !== 'collect') return
  
    let count = 1
    if (args.length === 3) count = parseInt(args[1])
  
    let type = args[1];
    if (args.length === 3) type = args[2]
  
    const blockType = mcData.blocksByName[type]
    if (!blockType) {
      bot.chat(`"I don't know any blocks named ${type}.`)
      return
    }
  
    const blocks = bot.findBlocks({
      matching: blockType.id,
      maxDistance: 64,
      count: count
    })

    console.log(blocks)
  
    if (blocks.length === 0) {
      bot.chat("I don't see that block nearby.")
      return
    }
  
    const targets = []
    for (let i = 0; i < Math.min(blocks.length, count); i++)
      targets.push(bot.blockAt(blocks[i]))
  
    bot.chat(`Found ${targets.length} ${type}(s)`)
  
    bot.collectBlock.collect(targets, err => {
      if (err) {
        // An error occurred, report it.
        bot.chat(err.message)
        console.log(err)
      } else {
        // All blocks have been collected.
        bot.chat('Done')
      }
    })
})

function follow () {
    const stopDistance = 3

    const playerToFollow = bot.players[target]

   
    if (playerToFollow.username === process.argv[4] || playerToFollow.username === 'gebbot') {
        bot.chat("No player to follow")
        return
    } else if (!playerToFollow || !playerToFollow.entity) {
        bot.chat("I can't see " + target)
        return
    } else if (!followUser) {
        bot.chat("No longer following " + target)
        bot.pathfinder.setGoal(new GoalFollow(bot.entity, 1))
    } else {
        bot.chat("Now Following " + target)
        const movements = new Movements(bot, mcData)
        movements.scafoldingBlocks = []
        movements.digCost = 99

        bot.pathfinder.setMovements(movements)

        const goal = new GoalFollow(playerToFollow.entity, stopDistance)
        bot.pathfinder.setGoal(goal, true)    
    }
}

function hi () {
    bot.chat('Hello World')
}
bot.on('chat', (username, message) => {
    if (username === bot.username) return
    if (message === 'follow me') {
        target = username
        followUser = true
        follow()
    }
    if (message === 'stop following me') {
        followUser = false
        follow()
    }
    if (message === 'find table') {
        findCraftingTable()
    }
    if (message === 'check tools') {
        checkTools()
    }
    if (message === 'get tools') {
        getTools()
    }
})

var tableFound = false

function findCraftingTable () {
  const craftingTableId = mcData.blocksByName.crafting_table
  const tablePos = bot.findBlocks({
      matching: craftingTableId.id,
      maxDistance: 32
  })
  if(tablePos.length === 0) {
      bot.chat('No Crafting Table Found')
  } else {
      bot.chat('Crafting Table Found')
      console.log('Crafting table at ' + tablePos)
      tableFound = true
  }
}

var furnaceFound = false

function findFurnace () {
  const furnace = mcData.blocksByName.furnace
  const furnacePos = bot.findBlocks({
      matching: furnace.id,
      maxDistance: 32
  })
  if(furnacePos.length === 0) {
      bot.chat('No Furnace Found')
  } else {
      bot.chat('Furnace Found')
      console.log('Furnace at ' + furnacePos)
      furnaceFound = true
  }
}

var stick = 0
var wood = 0
var stone = 0
var iron = 0
var diamond = 0
var logs = 0

function checkTools () {
  stick = 0
  wood = 0
  stone = 0
  iron = 0
  diamond = 0
  logs = 0
  checkInventoryFor('diamond_pickaxe', 1, () => {
      diamond = diamond + 3
      stick = stick + 2
      checkInventoryFor('iron_pickaxe', 1, () => {
          iron = iron + 3
          stick = stick + 2
          checkInventoryFor('stone_pickaxe', 1, () => {
              stone = stone + 3
              stick = stick + 2
              checkInventoryFor('wooden_pickaxe', 1, () => {
                stick = stick + 2
                wood = wood + 3
              })
          })
      })
  })
  checkInventoryFor('diamond_axe', 1, () => {
    diamond = diamond + 3    
    stick = stick + 2
  })
  checkInventoryFor('diamond_shovel', 1, () => {
  diamond = diamond + 1
  stick = stick + 2
  })
  checkInventoryFor('diamond_hoe', 1, () => {
  diamond = diamond + 2
  stick = stick + 2
  })
  tableFound = false
  findCraftingTable() 
    if (!tableFound) {
        wood = wood + 4
  }
  furnaceFound = false
  findFurnace() 
    if (!furnaceFound) {
        stone = stone + 8
  }
    wood = wood + (2 * Math.ceil(stick / 4))
    console.log('Wood needed = ' + wood)
    console.log('Stone needed = ' + stone)
    console.log('Iron needed = ' + iron)
    console.log('Diamond needed = ' + diamond)
    logs = Math.ceil(wood / 4)
    console.log('Logs needed = ' + logs) 
}

function getTools() {
  bot.chat('Getting Tools')
  checkTools()
  bot.chat('collect ' + logs + ' oak_log')
  bot.on('chat', (username, message) => {
    if(username === bot.username) {
      if(message === 'Done') {
        if(!tableFound) {
          bot.unequip("hand", () => {
            craftItem('oak_planks', logs, () => {
              craftItem('crafting_table', 1, () => {
                bot.equip(mcData.findItemOrBlockByName('crafting_table').id, "hand", () => {
                  placeBlockAbove(bot.entity.position.offset(1,0,1), () => {craftTools()})
                })
              })
            }) 
          })
        }
      }
    }
  })
}

function craftTools () {
  checkInventoryFor('stick', stick, () => {
    craftItem('stick', Math.ceil(stick / 4))
  })
  checkInventoryFor('diamond', diamond, () => {
    //if iron bars, smelt stuff
    //if gold bars, smelt stuff
    //if netherite bars, craft stuff
    //if planks, craft stuff
    //if diamond, mine stuff
  }, () => {
    craftItem('diamond_pickaxe', 1)
  })
}

function placeBlockAbove(targetSpot, callback = () => {console.log('successfully placed block')}) {
  targetSpot = bot.blockAt(targetSpot)
  referenceBlock = bot.blockAt(targetSpot.position.offset(0, -1, 0))
  const air = mcData.blocksByName.air.id
  if (targetSpot.id !== mcData.blocksByName.air ||targetSpot !== 'null') {
    bot.dig(targetSpot, () => {
      bot.placeBlock(referenceBlock, new Vec3(0, 1, 0)) 
    })
    callback()
  } else {
    bot.placeBlock(referenceBlock, new Vec3(0, 1, 0))
  }
}

function checkInventoryFor (itemName, amount, onFail = () => {console.log('No ' + itemName + ' found')}, onSuccess = () => {console.log(itemName + ' found')}) {
  amount = parseInt(amount, 10)
  const item = mcData.findItemOrBlockByName(itemName)
  if(bot.inventory.findInventoryItem(item.id, null)) {
    onSuccess()
  } else {    
    onFail()
  }
}

function craftItem (name, amount, onSuccess = () => {console.log('Crafted ' + amount + ' ' + name)}) {
  amount = parseInt(amount, 10)
  const item = mcData.findItemOrBlockByName(name)
  const craftingTable = bot.findBlock({
    matching: 58
  })

  if (item) {
    const recipe = bot.recipesFor(item.id, null, 1, craftingTable)[0]
    if (recipe) {
      bot.chat(`I can make ${name}`)
      bot.craft(recipe, amount, craftingTable, (err) => {
        if (err) {
          bot.chat(`error making ${name} at ${craftingTable}`)
        } else {
          bot.chat(`created ${name} ${amount} times`)
          onSuccess()
        }
      })
    } else {
      bot.chat(`I cannot make ${name}`)
    }
  } else {
    bot.chat(`unknown item: ${name}`)
  }
}