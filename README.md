# gebbot
A general purpose minecraft bot

Launch with command

  node gebbot.js \<server address\> \<server port\> [\<username\>] [\<password\>]

Once it has joined your server and said "Hello World", it will start accepting chat commands.

  Current Commands Supported
  
    get tools -- WIP, analyses current inventory and builds a list of what it needs to craft a full set of diamond tools
    
    collect [quantity] [item_name] -- Will look nearby for the specified quantity of an item, and then attemp to collect them
    
    follow me -- Follows the user who asked
    
    stop following me -- stops following the user who asked
